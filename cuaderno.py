import datetime

# Almacena todas las ids disponibles para las nuevas notas
ultima_id = 0


class Nota:
    """Representa una nota en el cuaderno. Se compara con un string en las busquedas y tambien permite poner etiquetas
    para cada nota"""

    def __init__(self, memo, tags=''):
        """incializa una nota como memo y opcional a tags separado por comas. Automaticamente configura la fecha de
        creacion  de la nota y una unica id"""
        self.memo = memo
        self.tags = tags
        self.creation_date = datetime.date.today()
        global ultima_id
        ultima_id += 1
        self.id = ultima_id

    def match(self, filter):
        """Compara contenido de la nota con el filtro de texto. Devuelve True o False. es Key-sensitive. Compara texto
        como tags"""
        return filter in self.memo or filter in self.tags
